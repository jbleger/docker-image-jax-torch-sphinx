FROM python:3.9
MAINTAINER Jean-Benoist Leger <jbleger@hds.utc.fr>

RUN pip install jax jaxlib torch numpy pytest sphinx sphinx_rtd_theme
